package com.example.nyc_schools.network

import com.example.nyc_schools.model.ResponseItem
import retrofit2.http.GET

interface SchoolsInterface {
    @GET("resource/f9bf-2cp4.json")
    suspend fun fetchSchoolsInfo(): List<ResponseItem>
}