package com.example.nyc_schools.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.nyc_schools.R
import com.example.nyc_schools.model.ResponseItem
import com.squareup.moshi.internal.Util

class SchoolAdapter(private val schoolList: List<ResponseItem?>?)  : RecyclerView.Adapter<SchoolAdapter.SchoolsViewHolder>() {

    var onSchoolClicked: ((TextView) -> Util)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolsViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.school_item, parent, false)
        return SchoolsViewHolder(view)
    }

    override fun onBindViewHolder(holder: SchoolsViewHolder, position: Int) {
        holder.schoolNameView.text = schoolList!![position]?.schoolName
    }

    override fun getItemCount(): Int = schoolList!!.size

    inner class SchoolsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val schoolNameView: TextView = itemView.findViewById(R.id.tvTitle)

        init {
            schoolNameView.setOnClickListener {
                onSchoolClicked?.invoke(it as TextView)
            }
        }

    }
}