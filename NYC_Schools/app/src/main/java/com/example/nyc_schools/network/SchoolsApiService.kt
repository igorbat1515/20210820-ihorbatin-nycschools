package com.example.nyc_schools.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.picasso.BuildConfig
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object SchoolsApiService {
    private val BASE_API = "https://data.cityofnewyork.us/"

    private val apiClient = OkHttpClient().newBuilder().build()

    private val retrofit = Retrofit.Builder().client(apiClient)
        .baseUrl(BASE_API)
        .addConverterFactory(MoshiConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()

    val schoolsApi: SchoolsInterface = retrofit.create(SchoolsInterface::class.java)
}