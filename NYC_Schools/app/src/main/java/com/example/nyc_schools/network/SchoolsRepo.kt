package com.example.nyc_schools.network

class SchoolsRepo(private val apiInterface: SchoolsInterface) {
    suspend fun getSchoolInfo() = apiInterface.fetchSchoolsInfo()
}