package com.example.nyc_schools

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.nyc_schools.adapters.SchoolAdapter
import com.example.nyc_schools.viewmodel.SchoolsViewModel

class MainActivity : AppCompatActivity() {
    private val schoolsViewModel: SchoolsViewModel by viewModels()
    private var schoolAdapter: SchoolAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupObservers()

        findViewById<RecyclerView>(R.id.rvSchools).apply {
            adapter = schoolAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
            itemAnimator = DefaultItemAnimator()
        }
    }

    override fun onResume() {
        super.onResume()

    }

    private fun setupObservers() {
        schoolsViewModel.schoolsLiveData.observe(this, Observer {
            schoolAdapter = SchoolAdapter(it)
        })
    }

}