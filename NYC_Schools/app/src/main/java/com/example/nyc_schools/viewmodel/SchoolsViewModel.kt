package com.example.nyc_schools.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.nyc_schools.network.SchoolsApiService
import com.example.nyc_schools.network.SchoolsRepo
import kotlinx.coroutines.Dispatchers

class SchoolsViewModel : ViewModel() {
    private val schoolsRepo = SchoolsRepo(SchoolsApiService.schoolsApi)

    val schoolsLiveData = liveData(Dispatchers.IO) {
        val data = schoolsRepo.getSchoolInfo()
        emit(data)
    }
}